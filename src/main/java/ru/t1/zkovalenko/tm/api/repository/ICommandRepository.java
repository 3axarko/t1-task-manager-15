package ru.t1.zkovalenko.tm.api.repository;

import ru.t1.zkovalenko.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalControls();

}
