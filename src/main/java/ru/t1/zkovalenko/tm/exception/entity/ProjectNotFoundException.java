package ru.t1.zkovalenko.tm.exception.entity;

public class ProjectNotFoundException extends AbstractEntityException {

    public ProjectNotFoundException() {
        super("Project not Found");
    }

}
