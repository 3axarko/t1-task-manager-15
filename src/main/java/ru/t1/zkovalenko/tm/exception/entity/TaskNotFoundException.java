package ru.t1.zkovalenko.tm.exception.entity;

public class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Task not Found");
    }

}
