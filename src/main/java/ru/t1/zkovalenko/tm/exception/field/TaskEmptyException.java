package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public class TaskEmptyException extends AbstractException {

    public TaskEmptyException() {
        super("Task is empty");
    }

}
