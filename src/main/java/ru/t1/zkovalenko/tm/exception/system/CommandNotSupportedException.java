package ru.t1.zkovalenko.tm.exception.system;

public class CommandNotSupportedException extends AbstractClassException {

    public CommandNotSupportedException() {
        super("Command not supported.");
    }

    public CommandNotSupportedException(String command) {
        super("Command '" + command + "' not supported.");
    }

}
